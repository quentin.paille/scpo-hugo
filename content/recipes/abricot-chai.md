+++
title = "abricot chai"
+++

Préparation : 5 minutes

Ingrédients (pour 2 verres)
50 cl de nectar d’abricot
2 c. à café de Chai L’Intense
Glaçons

Réalisation
Mélangez le nectar et le chai.

Versez sur des glaçons et servez.

Astuce
Pour un parfum de chai encore plus intense, utilisez les Glaçons au Chai !
